package logging

import (
	"fmt"
	"io"
	"os"

	el "github.com/labstack/gommon/log"
	zl "github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

// EchoLogger is a logger for echo based on Zerolog
type EchoLogger struct{}

// ------------------------------------------------------
// echo logger implementation

// Output returns Stderr
func (l EchoLogger) Output() io.Writer {
	return os.Stderr
}

// SetOutput does nothing
func (l EchoLogger) SetOutput(w io.Writer) {
	// not implemented
	log.Error().Msg("Unexpected call to EchoLogger SetOutput")
}

// Prefix returns an empty string
func (l EchoLogger) Prefix() string {
	log.Error().Msg("Unexpected call to EchoLogger Prefix")
	return ""
}

// SetPrefix does nothing
func (l EchoLogger) SetPrefix(p string) {
	// not implemented
	log.Error().Msgf("Unexpected call to EchoLogger SetPrefix `%s`", p)
}

// Level returns the level of the zerologger
func (l EchoLogger) Level() el.Lvl {
	return fromZeroLevel(log.Logger.GetLevel())
}

// SetLevel does nothing
func (l EchoLogger) SetLevel(v el.Lvl) {
	// not implemented
	log.Error().Msgf("Unexpected call to EchoLogger SetLevel `%d`", v)
}

// SetHeader does nothing
func (l EchoLogger) SetHeader(h string) {
	// not implemented
	log.Error().Msgf("Unexpected call to EchoLogger SetHeader `%s`", h)
}

// Print forwards to zerolog
func (l EchoLogger) Print(i ...interface{}) {
	log.WithLevel(zl.NoLevel).Msg(fmt.Sprint(i...))
}

// Printf forwards to zerolog
func (l EchoLogger) Printf(format string, args ...interface{}) {
	log.WithLevel(zl.NoLevel).Msgf(format, args...)
}

// Printj forwards to zerolog
func (l EchoLogger) Printj(j el.JSON) {
	logJSON(log.WithLevel(zl.NoLevel), j)
}

// Debug forwards to zerolog
func (l EchoLogger) Debug(i ...interface{}) {
	log.Debug().Msg(fmt.Sprint(i...))
}

// Debugf forwards to zerolog
func (l EchoLogger) Debugf(format string, args ...interface{}) {
	log.Debug().Msgf(format, args...)
}

// Debugj forwards to zerolog
func (l EchoLogger) Debugj(j el.JSON) {
	logJSON(log.Debug(), j)
}

// Info forwards to zerolog
func (l EchoLogger) Info(i ...interface{}) {
	log.Info().Msg(fmt.Sprint(i...))
}

// Infof forwards to zerolog
func (l EchoLogger) Infof(format string, args ...interface{}) {
	log.Info().Msgf(format, args...)
}

// Infoj forwards to zerolog
func (l EchoLogger) Infoj(j el.JSON) {
	logJSON(log.Info(), j)
}

// Warn forwards to zerolog
func (l EchoLogger) Warn(i ...interface{}) {
	log.Warn().Msg(fmt.Sprint(i...))
}

// Warnf forwards to zerolog
func (l EchoLogger) Warnf(format string, args ...interface{}) {
	log.Warn().Msgf(format, args...)
}

// Warnj forwards to zerolog
func (l EchoLogger) Warnj(j el.JSON) {
	logJSON(log.Warn(), j)
}

// Error forwards to zerolog
func (l EchoLogger) Error(i ...interface{}) {
	log.Error().Msg(fmt.Sprint(i...))
}

// Errorf forwards to zerolog
func (l EchoLogger) Errorf(format string, args ...interface{}) {
	log.Error().Msgf(format, args...)
}

// Errorj forwards to zerolog
func (l EchoLogger) Errorj(j el.JSON) {
	logJSON(log.Error(), j)
}

// Fatal forwards to zerolog
func (l EchoLogger) Fatal(i ...interface{}) {
	log.Fatal().Msg(fmt.Sprint(i...))
}

// Fatalj forwards to zerolog
func (l EchoLogger) Fatalj(j el.JSON) {
	logJSON(log.Fatal(), j)
}

// Fatalf forwards to zerolog
func (l EchoLogger) Fatalf(format string, args ...interface{}) {
	log.Fatal().Msgf(format, args...)
}

// Panic forwards to zerolog
func (l EchoLogger) Panic(i ...interface{}) {
	log.Panic().Msg(fmt.Sprint(i...))
}

// Panicj forwards to zerolog
func (l EchoLogger) Panicj(j el.JSON) {
	logJSON(log.Panic(), j)
}

// Panicf forwards to zerolog
func (l EchoLogger) Panicf(format string, args ...interface{}) {
	log.Panic().Msgf(format, args...)
}

// ------------------------------------------------------
// Utils

func fromZeroLevel(level zl.Level) el.Lvl {
	switch level {
	case zl.DebugLevel:
		return el.DEBUG
	case zl.InfoLevel:
		return el.INFO
	case zl.WarnLevel:
		return el.WARN
	case zl.ErrorLevel:
		return el.ERROR
	default:
		return el.OFF
	}
}

func logJSON(event *zl.Event, j el.JSON) {
	for k, v := range j {
		event = event.Interface(k, v)
	}

	event.Msg("")
}
