module gitlab.com/thegalabs/go/logging

go 1.16

require (
	github.com/getsentry/sentry-go v0.13.0
	github.com/labstack/echo/v4 v4.7.2
	github.com/labstack/gommon v0.3.1
	github.com/rs/zerolog v1.26.1
	github.com/stretchr/testify v1.7.1
)
