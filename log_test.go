package logging

import (
	"os"
	"testing"

	"github.com/rs/zerolog"
	"github.com/stretchr/testify/assert"
)

func TestSetGlobalLevel(t *testing.T) {
	generator := func(value string, expected zerolog.Level) func(t *testing.T) {
		return func(t *testing.T) {
			zerolog.SetGlobalLevel(zerolog.InfoLevel)

			os.Setenv("LOG_LEVEL", value)
			setLogLevelFromEnv()

			assert.Equal(t, expected, zerolog.GlobalLevel())
		}
	}

	t.Run("Empty", generator("", zerolog.InfoLevel))
	t.Run("Invalid", generator("hello", zerolog.InfoLevel))
	t.Run("Debug", generator("0", zerolog.DebugLevel))
	t.Run("Out of bounds", generator("1000", zerolog.InfoLevel))
}
